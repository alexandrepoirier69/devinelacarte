package org.sio.slam.devine


import org.junit.Assert
import org.junit.Test
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.fail
import org.sio.slam.devine.core.Carte
import org.sio.slam.devine.core.Paquet
import org.sio.slam.devine.enum.CouleurCarte
import org.sio.slam.devine.enum.NomCarte
import org.sio.slam.devine.fabrique.createJeu32Cartes
import org.sio.slam.devine.fabrique.createJeu52Cartes

internal class PaquetTest {

    @Test
    fun cardinal2Cartes() {
        val paquet2Cartes = Paquet(
            listOf(
                Carte(NomCarte.VALET, CouleurCarte.COEUR),
                Carte(NomCarte.DIX, CouleurCarte.TREFLE),
            )
        )
        assertEquals(2, paquet2Cartes.cardinal())
    }

    @Test
    fun testToString2Cartes() {
        val paquet2Cartes = Paquet(
            listOf(
                Carte(NomCarte.VALET, CouleurCarte.COEUR),
                Carte(NomCarte.DIX, CouleurCarte.TREFLE),
            )
        )
        assertEquals("Paquet de 2 cartes", paquet2Cartes.toString())
    }

    @Test
    fun testGetCartes() {
        val paquet2Cartes = Paquet(
            listOf(
                Carte(NomCarte.VALET, CouleurCarte.COEUR),
                Carte(NomCarte.DIX, CouleurCarte.TREFLE),
            )
        )
        var testCartes: List<Carte> = paquet2Cartes.cartes
        assertEquals(2, testCartes.size)
        assertEquals(NomCarte.VALET, testCartes[0].nom)
        assertEquals(CouleurCarte.COEUR, testCartes[0].couleur)
//        fail("test accesseur get cartes à implémenter")
    }

    @Test
    fun fabriqueDe32Cartes() {
        val test = Paquet(createJeu32Cartes())
        // TODO test fabriqueDe32Cartes à implémenter :
        //fail("fabriqueDe32Cartes à implémenter")
        assertEquals(32, test.cartes.size)
        assertEquals(NomCarte.SEPT, test.cartes[0].nom)
    }

    @Test
    fun fabriqueDe52Cartes() {
        // TODO test fabriqueDe52Cartes à implémenter :
        //fail("fabriqueDe52Cartes à implémenter")
        val test = Paquet(createJeu52Cartes())
        assertEquals(52, test.cartes.size)
    }

    @Test
    fun testCarteADeviner() {
        val paquet2Cartes = Paquet(
            listOf(
                Carte(NomCarte.VALET, CouleurCarte.COEUR),
                Carte(NomCarte.DIX, CouleurCarte.TREFLE),
            )
        )
        var test = false
        val rand = paquet2Cartes.getCarteADeviner().nom
        if (rand == NomCarte.VALET || rand == NomCarte.DIX) {
            test = true
        }
        assertTrue(test)
    }
    @Test
    fun melangeDeCartes(){
        val test = Paquet(createJeu52Cartes())
        assertEquals("DEUX",test.cartes[0].nom.toString())

        test.melange()
        assertNotEquals("DEUX",test.cartes[0].nom)
    }
}

