package org.sio.slam.devine

import org.sio.slam.devine.core.Carte
import org.sio.slam.devine.core.Jeu
import org.sio.slam.devine.core.Paquet
import org.sio.slam.devine.enum.CouleurCarte
import org.sio.slam.devine.enum.NomCarte
import org.sio.slam.devine.enum.getCouleurCarteFromString
import org.sio.slam.devine.enum.getNomCarteFromString
import org.sio.slam.devine.fabrique.createJeu32Cartes
import org.sio.slam.devine.fabrique.createJeu52Cartes

fun main(args: Array<String>) {
    var essaie: Int = 0
    var recommencer: Boolean = true
    var win:Boolean = false
    var avecAide: Boolean = false
    // TODO (A) demander au joueur s'il souhaite avoir de l'aide pour sa partie
    println("Voulez-vous de l'aide ? oui ou non: ")
    val questionAide = readLine().toString().trim().lowercase()
    if (questionAide == "oui") {
        println("Tu as pris de l'aide")
        avecAide = true
    } else {
        println("Tu n'as pas pris d'aide")
    }
    // TODO (A) demander au joueur avec quel jeu de cartes 32 ou 52 il souhaite jouer
    var paquetDeCartes = Paquet(createJeu32Cartes())
    do {
        println("Choisir le type de jeu, 32 ou 52 cartes: ")
        val nbCartePaquet = readLine()?.toInt()
        if (nbCartePaquet == 32) {
            paquetDeCartes = Paquet(createJeu32Cartes())
            println("Création d'un paquet de 32 cartes")
        } else if (nbCartePaquet == 52) {
            paquetDeCartes = Paquet(createJeu52Cartes())
            println("Création d'un paquet de 52 cartes")
        }
    } while (nbCartePaquet != 32 && nbCartePaquet != 52)

    println(" ==== Instanciation du jeu, début de la partie. ====")
    val jeu = Jeu(avecAide, paquetDeCartes)
    do {
        println("Entrez un nom de carte dans le jeu (exemples : Roi, sept, six, As...) :")
        // TODO (optionnel) permettre de saisir un chiffre au lieu d'une chaine : 7 au lieu de Sept...
        val nomCarteUserStr: String = readLine() + ""
        val nomCarteUser: NomCarte? =
            if (nomCarteUserStr in (2..10).toSet().toString()) {
                val words = arrayOf("deux", "trois", "quatre", "cinq", "six", "sept", "huit", "neuf", "dix")
                val nomCarteUserInt = nomCarteUserStr.toInt()
                val result = words[nomCarteUserInt - 2]
                getNomCarteFromString(result.trim().uppercase())
            } else
                getNomCarteFromString(nomCarteUserStr.trim().uppercase())

        println("Entrez un nom de \"couleur\" de carte parmi : Pique, Trefle, Coeur, Carreau : ")
        val couleurCarteUserStr: String = readLine() + ""
        val couleurCarteUser: CouleurCarte? = getCouleurCarteFromString(couleurCarteUserStr.trim().uppercase())
        essaie++
        if (nomCarteUser != null && couleurCarteUser != null) {
            // prise en compte de la carte du joueur
            val carteDuJoueur: Carte = Carte(nomCarteUser, couleurCarteUser)

            if (jeu.isMatch(carteDuJoueur)) {
                println("Bravo, vous avez trouvé la bonne carte !")
                recommencer = false
                win = true
            } else {
                println("Ce n'est pas la bonne carte !")
                println("votre proposition  : $carteDuJoueur")
                if (avecAide) {
                    // TODO: (A) si l'aide est activée, alors dire si la carte proposée est
                    //  plus petite ou plus grande que la carte à deviner
                    if (carteDuJoueur.valeur.compareTo(jeu.carteADeviner.valeur) == 0) {
                        println("la valeur est la meme")
                    }
                    if (carteDuJoueur.valeur.compareTo(jeu.carteADeviner.valeur) > 0) {
                        println("la valeur est plus petite")
                    }
                    if (carteDuJoueur.valeur.compareTo(jeu.carteADeviner.valeur) < 0) {
                        println("la valeur est plus grande")
                    }
                    if (carteDuJoueur.valCouleur.compareTo(jeu.carteADeviner.valCouleur) == 0) {
                        println("la couleur est la meme")
                    }
                    if (carteDuJoueur.valCouleur.compareTo(jeu.carteADeviner.valCouleur) > 0) {
                        println("la couleur est plus petite")
                    }
                    if (carteDuJoueur.valCouleur.compareTo(jeu.carteADeviner.valCouleur) < 0) {
                        println("la couleur est plus grande")
                    }
                }
            }
        } else {
            // utilisateur a mal renseigné sa carte
            val nomCarte = if (nomCarteUserStr == "") "?" else nomCarteUserStr
            val couleurCarte = if (couleurCarteUserStr == "") "?" else couleurCarteUserStr

            println("Désolé, mauvaise définition de carte ! (${nomCarte} de ${couleurCarte})")
        }
        if (!win) {
            println("recommencez ? : (oui/ non)")
            val reponse = readLine()
            if (reponse == "non") {
                recommencer = false
            }
        }
    } while (recommencer)
    // TODO (A) permettre au joueur de retenter une autre carte (sans relancer le jeu) ou d'abandonner la partie
    println(" ==== Fin prématurée de la partie ====")

    // TODO (A) Présenter à la fin la carte à deviner
    println("La carte à deviner est ${jeu.carteADeviner} ")

    // TODO (challenge-4) la stratégie de jeu est à implémenter... à faire lorsque les autres TODOs auront été réalisés
    println("Votre stratégie de jeu : ${jeu.strategiePartie(essaie)}")

    println(" ==== Fin de la partie. ====")
}